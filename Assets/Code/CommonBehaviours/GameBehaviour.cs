﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interfaces;

public class GameBehaviour : MonoBehaviour, IMonoBehaviour
{
    MonoBehaviour IMonoBehaviour.Mono { get { return this as MonoBehaviour; } }

    public virtual void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
    }
}
